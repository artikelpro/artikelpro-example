# ArtikelPro example project

To see this demo, please open ```src/index.html``` using a modern browser (e.g. Chrome)

## Disclaimer

In our examples we use JavaScript and curl snippets. All actions can also be done in any other programming language but as an example JavaScript and cURL are used since they have a commonly known syntax.

## How to use the ArtikelPro API

An OpenAPI description of this service is available in ```openapi.json```. Please see [the official specification](https://swagger.io/specification/) for more information on that standard.

When using the __deprecated__ ```v1``` version of the API, please checkout branch ```v1``` of this project for an example

### Authentication

All endpoints of the ArtikelPro API require an Authentication header in the requests.
As te content for this Authentication header we use JSON web tokens (JWT).

First of, you need to acquire a **client id** and a **secret token**. With these values you can create an JWT and fetch data from the API.

### JSON Web Tokens

JWT (json web token) is a method to securely communicate between two parties (for more information check [JWT.io](https://JWT.io/introduction/)).
The JWT consist of three sections, the header, the payload and the signature. All these sections are individually encoded and concatenated together to create one token.

## <span style="color:red">Important!</span>

Before we get into what the content of the JWT should be, we should talk about the TTL (time to live) of the JWT's.
For security purposes can a JWT only be used for **one** day. This means that you will have to create a new JWT token every day.
If you use an 'old' JWT, for example the JWT you created yesterday, you won't be able to fetch any content from the API. 
We validate the 'valid date' using the `dat` value of the JWT payload.

##### JWT content

First of, the header. In the header we provide the information about the used encoding algorithm. In our case it will look like this:
```js
{ alg: "HS256", typ: "JWT" }
```

Next of, the payload. This is where we provide information about who we are, when the token was created and which API we would like to communicate with. In this case the payload will like something like this:
In this case the payload will like something like this:
```js
{
  iss: "https://artikelpro.eu/",
  sub: YOUR_CLIENT_ID,
  aud: "https://data.artikelpro.eu/",
  dat: CURRENT_DATE_IN_YYMMDD_FORMAT,
  gty: "client-credentials"
}
```

`YOUR_CLIENT_ID` This is the **client id** you received from ArtikelPro.
`CURRENT_DATE_IN_YYMMDD_FORMAT` This is the current date, formatted as YYMMDD (so a date like 17 February, 2020 will look like "20200217"). This is used to determine if the JWT is still valid (see the **Important** section).


And finally the signature, this is used to encode and decode the JWT. Here we use the **secret token** you received from ArtikelPro.

For this project we used a library called [jsrsasign](https://kjur.github.io/jsrsasign/). 
Documentation and example code can be found [here](https://github.com/kjur/jsrsasign/wiki/Tutorial-for-JWS-generation)

### Fetching content

Once you have created your JWT token you are ready to start fetching the data. This can be done with the following curl command:

```
curl --request GET \
  --url https://data.artikelpro.eu/v2/contentResult/crud \
  --header 'content-type: application/json' \
  --header 'authorization: Bearer YOUR_ACCESS_TOKEN'
```

This request will return a collection of items in a json notation, and will look similar to this:

```json
[
  {
    "contentId": 156842,
    "queryName": "Lorum",
    "queryType": "profile",
    "customer": " Ipsum",
    "reference": "",
    "source": "My newspaper",
    "title": "What is Lorem Ipsum?",
    "publicationDateTime": "2022-11-30T00:00:00.000+01:00",
    "contentGroup": "Tijdschriften",
    "mediaType": "print",
    "insertDateTime": "2023-01-11T18:41:25.847+01:00",
    "firstPageNumber": 26,
    "body": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...",
    "snippet": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
    "link": "https://link.to/article",
    "checkedDateTime": "2023-01-11T18:41:25.847+01:00",
    "checkedBy": "Automatic"
  },
  ...
]
```

Please consult  ```openapi.json``` for more information on there specific fields.
