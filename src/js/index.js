if (localStorage.getItem("jwt")) {
  window.location = "result.html";
}

document.querySelector("form").addEventListener("submit", (e) => {
  e.preventDefault();
  // 1. Retrieve form values...
  const sub = document.querySelector('[name="form__auth_token"]').value;
  const secret = document.querySelector('[name="form__auth_secret"]').value;

  // 2. Setup date variables
  const date = new Date();
  const day = date.getDate().toString();
  const month = (date.getMonth() + 1).toString();

  // 3. Contruct JWT Auth token via RSA Sign libary, in this example https://github.com/kjur/jsrsasign/
  localStorage.setItem(
    "jwt",
    KJUR.jws.JWS.sign(
      "HS256",
      JSON.stringify({ alg: "HS256", typ: "JWT" }),
      JSON.stringify({
        iss: "https://artikelpro.eu/",
        sub,
        aud: "https://data.artikelpro.eu/",
        dat: `${date.getFullYear()}${month.length < 2 ? "0" : ""}${month}${
          day.length < 2 ? "0" : ""
        }${day}`,
        gty: "client-credentials",
      }),
      secret
    )
  );

  // 4. Try and load results in results page
  window.location = "result.html";
});
