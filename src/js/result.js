// GUI table configuration
const tableColumns = {
  title: "Titel",
  body: "Content",
  queryName: "Profiel",
  mediaType: "Media type",
  source: "Bron",
  insertDateTime: "Gevonden op",
  publicationDateTime: "Publicatie datum",
  link: "Link",
  checkedBy: "Gecontroleerd door",
  checkedDateTime: "Gecontroleerd op",
  contentGroup: "Soort",
  customer: "Klant",
  firstPageNumber: "Pagina"
};

// Find DOM elements for data binding
const maxResultsCountInput = document.querySelector('[name="maxResultsCount"]');
const batchIdInput = document.querySelector('[name="batchId"]');
const form = document.querySelector("form");
const header = document.getElementsByTagName('h1')[0];

const tableBody = document.getElementById("table-body");
document.getElementById("logoutButton").addEventListener("click", () => {
  localStorage.removeItem("jwt");
  window.location = "index.html";
});

// Retrieve openapi.json to get server URL
const jwt = localStorage.getItem("jwt");

// Set default maxResultCount, based on schema
maxResultsCountInput.value =
  localStorage.getItem("maxResultsCount") ||
  "50";


form.addEventListener("submit", (e) => {
  e.preventDefault();
  const maxResultsCount = maxResultsCountInput.value || "50";
  const batchId = batchIdInput.value || "";
  fetch(
    `https://data.artikelpro.eu/v2/contentResult/crud?${new URLSearchParams({
      maxResultsCount,
      batchId,
    })}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${jwt}`,
      },
    }
  )
    .then((res) => res.json())
    .then((res) => {
      const data = res.results;
      // 1. If there are new results, update localstorage
      if (data.length < 1) {
        window.alert("Er zijn geen resultaten...");
        return;
      }
      batchIdInput.value = batchId;
      header.innerHTML = `Content resultaten (Batch: ${res.batchId}, Resterende resultaten: ${res.remainingCount})`;
      // 2. Start rendering the results
      tableBody.innerHTML = "";
      const columnNames = Object.keys(tableColumns);
      // Create a table row for every record
      data.forEach((object) => {
        const row = document.createElement("tr");
        columnNames.forEach((propName) => {
          const tableCell = document.createElement("td");
          switch (propName) {
            case "body":
              tableCell.innerText =
                object[propName].length > 150
                  ? `${object[propName].substr(0, 150)}...`
                  : object[propName];
              break;

            case "checkedDateTime":
            case "publicationDateTime":
              tableCell.innerText = new Date(
                object[propName]
              ).toLocaleDateString();
              break;

            case "link":
              tableCell.innerHTML = `<a href="${object.link}" target="_blank">${object.link}</a>`;
              break;

            default:
              tableCell.innerText = object[propName];
          }
          row.append(tableCell);
        });
        tableBody.append(row);
      });
    })
    .catch((err) => {
      console.log(err);
      localStorage.removeItem("jwt");
      window.alert("Data retrieval failed. Please check API credentials");
      window.location = "index.html";
    });
});
